// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VueAuth from '@websanova/vue-auth'
import VueMoment from 'vue-moment'

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import './assets/prism/prism.css'
// import './assets/prism/prism'
// import VueBreadcrumbs from 'vue-breadcrumbs'

Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(VueMoment)
Vue.use(mavonEditor)

// Vue.use(VueBreadcrumbs)
// local_server:'http://localhost:3000/'
// remote_server: 'http://protons-classroom.herokuapp.com'
let endPoint = process.env.API_END_POINT || 'https://protons-classroom.herokuapp.com'
Vue.http.options.root = endPoint

Vue.router = router

Vue.use(VueAuth, {
  auth: require('@websanova/vue-auth/drivers/auth/devise'),
  http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x'),
  loginData: { url: `${endPoint}/auth/sign_in`, method: 'POST', redirect: '/', fetchUser: true },
  registerData: { url: `${endPoint}/auth`, method: 'POST', redirect: '/', fetchUser: true },
  logoutData: { url: `${endPoint}/auth/sign_out`, method: 'DELETE', redirect: '/login', makeRequest: true },
  fetchData: { url: `${endPoint}/auth/validate_token`, method: 'GET', enabled: true },
  refreshData: { url: `${endPoint}/auth/validate_token`, method: 'GET', enabled: true }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

router.beforeEach(
  (to, from, next) => {
    if (from.path === '/login') {
      to.params.fromLogin = true
    }
    next()
  }
)
