import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login/Login'
import SessionDetails from '@/components/Sessions/SessionDetails'
import Sessions from '@/components/Sessions/Sessions'
import Home from '@/components/Home/Home'
import Assignments from '@/components/Assignments/Assignments'
import AssignmentDetails from '@/components/Assignments/AssignmentDetails'
import Questions from '@/components/Forum/Questions'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: { auth: true }
    },
    {
      path: '/assignments',
      name: 'Assignments',
      component: Assignments,
      meta: { auth: true }
    },
    {
      path: '/sessions/:id',
      name: 'SessionDetails',
      component: SessionDetails,
      meta: { auth: true }
    },
    {
      path: '/sessions',
      name: 'Sessions',
      component: Sessions,
      meta: { auth: true }
    },
    {
      path: '/assignments/:id',
      name: 'AssignmentDetails',
      component: AssignmentDetails,
      meta: { auth: true }
    },
    {
      path: '/forum',
      name: 'Forum',
      component: Questions,
      meta: { auth: true }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { auth: false }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
